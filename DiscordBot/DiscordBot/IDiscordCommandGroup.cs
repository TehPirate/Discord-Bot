﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot
{
    public interface IDiscordCommandGroup
    {
        string gName { get; set; }
        //Task Run(CommandEventArgs e);
        Task Run(CommandService e);
    }
}