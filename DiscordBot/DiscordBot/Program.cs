﻿using Discord;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Modules;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot
{
    class Program
    {
        private static void Main(string[] args) => new Program().Start(args);
        public DiscordClient client;
        private void Start(string[] args)
        {
            client = new DiscordClient(x =>
            {
                x.AppName = ConfigurationManager.AppSettings["AppName"];
                x.LogLevel = LogSeverity.Info;
                x.LogHandler = OnLogMessage;
            })
            .UsingCommands(x =>
            {
                x.AllowMentionPrefix = true;
                x.HelpMode = HelpMode.Public;
                x.ExecuteHandler = OnCommandExecuted;
                x.ErrorHandler = OnCommandError;
                x.PrefixChar = Convert.ToChar("~");
            })
            .UsingPermissionLevels((u, c) => (int)GetPermissions(u, c))
            .UsingModules();
            RegisterGroups(client.GetService<CommandService>());
            RegisterCommands(client.GetService<CommandService>());
            client.ExecuteAndWait(async () =>
            {
                try
                {
                    await client.Connect(ConfigurationManager.AppSettings["token"]);
                    client.SetGame("with Discord.Net");
                    Console.WriteLine(String.Format("User: {0} {3}id: {1} {3}verified: {2}", client.CurrentUser.Name, client.CurrentUser.Id, client.CurrentUser.IsVerified, Environment.NewLine));
                }
                catch (Exception ex)
                {
                    client.Log.Error($"Login Failed", ex);
                    await Task.Delay(client.Config.FailedReconnectDelay);
                }

            });
        }

        private void OnLogMessage(object sender, LogMessageEventArgs e)
        {
            //Color
            ConsoleColor color;
            switch (e.Severity)
            {
                case LogSeverity.Error: color = ConsoleColor.Red; break;
                case LogSeverity.Warning: color = ConsoleColor.Yellow; break;
                case LogSeverity.Info: color = ConsoleColor.White; break;
                case LogSeverity.Verbose: color = ConsoleColor.Gray; break;
                case LogSeverity.Debug: default: color = ConsoleColor.DarkGray; break;
            }

            //Exception
            string exMessage;
            Exception ex = e.Exception;
            if (ex != null)
            {
                while (ex is AggregateException && ex.InnerException != null)
                    ex = ex.InnerException;
                exMessage = ex.Message;
            }
            else
                exMessage = null;

            //Source
            string sourceName = e.Source?.ToString();

            //Text
            string text;
            if (e.Message == null)
            {
                text = exMessage ?? "";
                exMessage = null;
            }
            else
                text = e.Message;

            //Build message
            StringBuilder builder = new StringBuilder(text.Length + (sourceName?.Length ?? 0) + (exMessage?.Length ?? 0) + 5);
            if (sourceName != null)
            {
                builder.Append('[');
                builder.Append(sourceName);
                builder.Append("] ");
            }
            for (int i = 0; i < text.Length; i++)
            {
                //Strip control chars
                char c = text[i];
                if (!char.IsControl(c))
                    builder.Append(c);
            }
            if (exMessage != null)
            {
                builder.Append(": ");
                builder.Append(exMessage);
            }

            text = builder.ToString();
            Console.ForegroundColor = color;
            Console.WriteLine(text);
        }

        private void OnCommandError(object sender, CommandErrorEventArgs e)
        {
            string msg = e.Exception?.Message;
            if (msg == null) //No exception - show a generic message
            {
                switch (e.ErrorType)
                {
                    case CommandErrorType.Exception:
                        msg = "Unknown error.";
                        break;
                    case CommandErrorType.BadPermissions:
                        msg = "You do not have permission to run this command.";
                        break;
                    case CommandErrorType.BadArgCount:
                        msg = "You provided the incorrect number of arguments for this command.";
                        break;
                    case CommandErrorType.InvalidInput:
                        msg = "Unable to parse your command, please check your input.";
                        break;
                    case CommandErrorType.UnknownCommand:
                        msg = "Unknown command.";
                        break;
                }
            }
            if (msg != null)
            {
                //client.ReplyError(e, msg);
                e.Channel.SendMessage(e + ": " + msg);
                client.Log.Error("Command", msg);
            }
        }

        private void OnCommandExecuted(object sender, CommandEventArgs e)
        {
            client.Log.Info("Command", $"{e.Command.Text} ({e.User.Name})");
            
        }
        public static void RegisterGroups(CommandService commandService)
        {
            Console.WriteLine("Registering groups...");

            var instances = from t in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(IDiscordCommandGroup))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as IDiscordCommandGroup;

            foreach (var instance in instances)
            {
                instance.Run(commandService);
            }

            Console.WriteLine(String.Format("{0} groups registered.", instances.Count()));
        }

        public static void RegisterCommands(CommandService commandService)
        {
            Console.WriteLine("Registering commands...");

            var instances = from t in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(IDiscordCommand))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as IDiscordCommand;

            foreach (var obj in instances)
            {
                if (obj.Args != null)
                {
                    if (obj.Alias != null)
                    {
                        commandService.CreateCommand(obj.Name)
                            .MinPermissions((int)obj.Permissions)
                            .Alias(new string[] { obj.Alias })
                            .Description(obj.Description)
                            .Parameter(obj.Args, obj.Paramater)
                            .Do(async e => await obj.Run(e));
                    }
                    else
                    {
                        commandService.CreateCommand(obj.Name)
                            .MinPermissions((int)obj.Permissions)
                            .Description(obj.Description)
                            .Parameter(obj.Args, obj.Paramater)
                            .Do(async e => await obj.Run(e));
                    }
                }
                else
                {
                    if (obj.Alias != null)
                    {
                        commandService.CreateCommand(obj.Name)
                            .MinPermissions((int)obj.Permissions)
                            .Alias(new string[] { obj.Alias })
                            .Description(obj.Description)
                            .Do(async e => await obj.Run(e));
                    }
                    else
                    {
                        commandService.CreateCommand(obj.Name)
                            .MinPermissions((int)obj.Permissions)
                            .Description(obj.Description)
                            .Do(async e => await obj.Run(e));
                    }
                }
            }
            Console.WriteLine(String.Format("{0} commands registered.", commandService.AllCommands.Count()));
        }

        private static PermissionLevel GetPermissions(User u, Channel c)
        {
            if (u.Id == Convert.ToUInt64(ConfigurationManager.AppSettings["ownerID"]))
                return PermissionLevel.BotOwner;

            if (!c.IsPrivate)
            {
                if (u == c.Server.Owner)
                    return PermissionLevel.ServerOwner;

                var serverPerms = u.ServerPermissions;
                if (serverPerms.ManageRoles || u.Roles.Select(x => x.Name.ToLower()).Contains("bot commander"))
                    return PermissionLevel.ServerAdmin;
                if (serverPerms.ManageMessages && serverPerms.KickMembers && serverPerms.BanMembers)
                    return PermissionLevel.ServerModerator;

                var channelPerms = u.GetPermissions(c);
                if (channelPerms.ManagePermissions)
                    return PermissionLevel.ChannelAdmin;
                if (channelPerms.ManageMessages)
                    return PermissionLevel.ChannelModerator;
            }
            return PermissionLevel.User;
        }
    }
}
