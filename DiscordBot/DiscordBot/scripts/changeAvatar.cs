﻿using Discord.Commands;
using System.Threading.Tasks;

namespace DiscordBot.scripts
{
    class changeAvatar : IDiscordCommand
    {
        public string Name { get; set; } = "avatar";
        public string Alias { get; set; } = null;
        public string Description { get; set; } = "Change the bot's avatar.";
        public ParameterType Paramater { get; set; } = ParameterType.Required;
        public string Args { get; set; } = "url";
        public int Permissions { get; set; } = (int)PermissionLevel.BotOwner;
        public async Task Run(CommandEventArgs e)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            wc.DownloadFile(e.GetArg("url"), "avatar.png");
            await e.User.Client.CurrentUser.Edit(avatar: new System.IO.FileStream(@"avatar.png", System.IO.FileMode.Open), avatarType: Discord.ImageType.Png);
        }
    }
}
