﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts.info
{
    class core : IDiscordCommandGroup
    {
        public string gName { get; set; } = "info";

        public async Task Run(CommandService commandService)
        {
            var instances = from t in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(IinfoGroup))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as IinfoGroup;

            commandService.CreateCommand("info")
                        .Description("Parent info command.")
                        .Do(async e => await e.Channel.SendMessage("Incorrect usage, see ~help info"));

            commandService.CreateGroup("info", cgb =>
            {
                foreach (var command in instances)
                {

                    if (command.Args != null)
                    {
                        if (command.Alias != null)
                        {
                            cgb.CreateCommand(command.Name)
                                .Alias(new string[] { command.Alias })
                                .Description(command.Description)
                                .Parameter(command.Args, command.Paramater)
                                .Do(async e => await command.Run(e));
                        }
                        else
                        {
                            cgb.CreateCommand(command.Name)
                                .Description(command.Description)
                                .Parameter(command.Args, command.Paramater)
                                .Do(async e => await command.Run(e));
                        }
                    }
                    else
                    {
                        if (command.Alias != null)
                        {
                            cgb.CreateCommand(command.Name)
                                .Alias(new string[] { command.Alias })
                                .Description(command.Description)
                                .Do(async e => await command.Run(e));
                        }
                        else
                        {
                            cgb.CreateCommand(command.Name)
                                .Description(command.Description)
                                .Do(async e => await command.Run(e));
                        }
                    }
                }

            });
        }
    }
}
