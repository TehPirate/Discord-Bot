﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts.info
{
    class other : IinfoGroup
    {
        public string Name { get; set; } = "other";
        public string Alias { get; set; } = null;
        public string Description { get; set; } = "Presents detailed information about another user.";
        public ParameterType Paramater { get; set; } = ParameterType.Required;
        public string Args { get; set; } = "Name";
        public async Task Run(CommandEventArgs e)
        {
            UInt64 id;
            bool isID = UInt64.TryParse(e.GetArg("Name"), out id);
            var user = isID ? e.Server.GetUser(id) : e.Server.FindUsers(e.GetArg("Name")).FirstOrDefault();
            try
            {
                await e.Channel.SendMessage(String.Format("```python" +
                    "\nName: {0} {9}" +
                    "ID: {1} {9}" +
                    "Previous Names: {2} {9}" +
                    "Created: {3} {9}" +
                    "Joined: {4} {9}" +
                    "Last Seen: {5} {9}" +
                    "Last Spoke: {6} {9}" +
                    "Roles: {7} {9}" +
                    "Avatar: {8} {9}" +
                    "```",
                    user,
                    user.Id,
                    user.Name,
                    new DateTime(2015, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(user.Id >> 22),
                    user.JoinedAt,
                    user.LastOnlineAt,
                    user.LastActivityAt,
                    String.Join(", ", user.Roles
                            .Where(o => o != e.Server.EveryoneRole)
                            .Select(o => o.ToString())),
                    user.AvatarUrl,
                    Environment.NewLine));
            } catch (Exception ex)
            {
                await e.Channel.SendMessage("User was not found. I can only discover users within the same server I was summoned on.");
            }
        }
    }
}
