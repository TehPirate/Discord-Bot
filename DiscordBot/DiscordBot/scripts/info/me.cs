﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts.info
{
    class me : IinfoGroup
    {
        public string Name { get; set; } = "me";
        public string Alias { get; set; } = null;
        public string Description { get; set; } = "Presents detailed information about you.";
        public ParameterType Paramater { get; set; } = ParameterType.Optional;
        public string Args { get; set; } = null;
        public async Task Run(CommandEventArgs e)
        {
            await e.Channel.SendMessage(String.Format("```python" +
                "\nName: {0} {9}" +
                "ID: {1} {9}" +
                "Previous Names: {2} {9}" +
                "Created: {3} {9}" +
                "Joined: {4} {9}" +
                "Last Seen: {5} {9}" +
                "Last Spoke: {6} {9}" +
                "Roles: {7} {9}" +
                "Avatar: {8} {9}" +
                "```",
                e.User,
                e.User.Id,
                e.User.Name,
                new DateTime(2015, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(e.User.Id >> 22),
                e.User.JoinedAt,
                e.User.LastOnlineAt,
                e.User.LastActivityAt,
                String.Join(", ", e.User.Roles
                        .Where(o => o != e.Server.EveryoneRole)
                        .Select(o => o.ToString())),
                e.User.AvatarUrl,
                Environment.NewLine));
        }
    }
}
