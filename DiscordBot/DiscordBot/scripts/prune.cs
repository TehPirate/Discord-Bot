﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts
{
    class prune : IDiscordCommand
    {
        public string Name { get; set; } = "prune";
        public string Alias { get; set; } = null;
        public string Description { get; set; } = "Removes last x messages.";
        public ParameterType Paramater { get; set; } = ParameterType.Optional;
        public string Args { get; set; } = "number";
        public int Permissions { get; set; } = (int)PermissionLevel.ChannelAdmin;
        public async Task Run(CommandEventArgs e)
        {
            int amount = Convert.ToInt32(e.GetArg("number"));
            var a = new List<string>();
            var messages = await e.Channel.DownloadMessages();
            foreach (var message in messages)
            {
                a.Add(message.Id.ToString());
            }
            var toRemove = a.Take(amount);
            foreach (string delete in toRemove)
            {
                await e.Channel.GetMessage(Convert.ToUInt64(delete)).Delete();
            }
        }
    }
}
