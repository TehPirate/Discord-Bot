﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts
{
    class playing : IDiscordCommand
    {
        public string Name { get; set; } = "playing";
        public string Alias { get; set; } = null;
        public string Description { get; set; } = "Change the game currently playing";
        public ParameterType Paramater { get; set; } = ParameterType.Optional;
        public string Args { get; set; } = "Game";
        public int Permissions { get; set; } = (int)PermissionLevel.BotOwner;
        public async Task Run(CommandEventArgs e)
        {
            e.User.Client.SetGame(e.GetArg("Game"));
        }
    }
}
