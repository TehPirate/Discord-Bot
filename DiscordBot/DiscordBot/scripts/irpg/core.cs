﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts.idlerpg
{
    public class core : IDiscordCommandGroup
    {
        public string gName { get; set; } = "irpg";
        public async Task Run(CommandService commandService)
        {
            var instances = from t in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(IirpgGroup))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as IirpgGroup;

            commandService.CreateGroup("irpg", cgb =>
            {
                foreach (var command in instances)
                {
                    cgb.CreateCommand(command.Name)
                        .Description(command.Description)
                        .Do(async e => await command.Run(e));
                }

            });
        }
    }
}
