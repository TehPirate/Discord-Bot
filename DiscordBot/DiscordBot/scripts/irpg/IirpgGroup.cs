﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts.idlerpg
{
    public interface IirpgGroup
    {
        string Name { get; set; }
        string Alias { get; set; }
        string Description { get; set; }
        ParameterType Paramater { get; set; }
        string Args { get; set; }
        int Permissions { get; set; }
        Task Run(CommandEventArgs e);
    }
}
