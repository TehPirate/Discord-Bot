﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts
{
    public class test : IDiscordCommand
    {
        public string Name { get; set; } = "test";
        public string Alias { get; set; } = null;
        public string Description { get; set; } = "This is a test command.";
        public ParameterType Paramater { get; set; } = ParameterType.Optional;
        public string Args { get; set; } = null;
        public int Permissions { get; set; } = (int)PermissionLevel.User;
        public async Task Run(CommandEventArgs e)
        {
            await e.Channel.SendMessage("Test Successful!");
        }
    }
}
