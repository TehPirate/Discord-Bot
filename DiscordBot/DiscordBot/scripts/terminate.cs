﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.scripts
{
    class terminate : IDiscordCommand
    {
        public string Name { get; set; } = "terminate";
        public string Alias { get; set; } = null;
        public string Description { get; set; } = "Stops the bot.";
        public ParameterType Paramater { get; set; } = ParameterType.Optional;
        public string Args { get; set; } = null;
        public int Permissions { get; set; } = (int)PermissionLevel.ServerAdmin;
        public async Task Run(CommandEventArgs e)
        {
            e.Channel.SendMessage("Shutting down threads and terminating...");
            e.Channel.SendMessage(":weary: :gun: bye");
            System.Threading.Thread.Sleep(500); 
            e.User.Client.Disconnect();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}
