﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot
{
    class Groups
    {
        public List<IDiscordCommandGroup> gList()
        {
            List<IDiscordCommandGroup> groups = new List<IDiscordCommandGroup>();
            groups.Add(new scripts.info.core());
            //groups.Add(new scripts.idlerpg.core());
            return groups;
        }
    }

    class Commands
    {
        public List<IDiscordCommand> cList()
        {
            List<IDiscordCommand> commands = new List<IDiscordCommand>();
            commands.Add(new scripts.test());
            commands.Add(new scripts.playing());
            commands.Add(new scripts.prune());
            commands.Add(new scripts.changeAvatar());
            commands.Add(new scripts.terminate());
            return commands;
        }
    }
}
